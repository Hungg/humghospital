package com.service;

import com.entity.Doctor;
import com.entity.Patients_history;
import com.exception.BadRequestException;
import com.model.PatientsHistoryDto;
import com.repository.*;
import com.util.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class PatientsHistoryService extends AbstractService<Patients_history, Long> {
    @Autowired
    private PatientsHistoryRepository patientsHistoryRepository;
    private DoctorRepository doctorRepository;
    private MedicineRepository medicineRepository;
    private PatientsRepository patientsRepository;
    private DiseasesRepository diseasesRepository;
    @Override
    protected JpaRepository<Patients_history, Long> getRepository() {
        return patientsHistoryRepository;
    }

//    public List<PatientsHistoryDto> changeType(Integer pageNumber, Integer limit, Date fromDate, Date toDate) {
//        Pageable pageable = PageRequest.of(pageNumber - 1, limit, Sort.Direction.DESC, "create_time");
//        Page<Patients_history> pageHistory = patientsHistoryRepository.getList(fromDate, toDate, pageable);
//
//        List<Patients_history> data = pageHistory.getContent();
//        List<PatientsHistoryDto> dto = null;
//        for(Patients_history ph: data) {
//            patientsHistoryDto.setDoctorName(doctorRepository.getByDoctorCode(ph.getDoctor_code()).getName());
//            patientsHistoryDto.setDiseasesName(diseasesRepository.getByDiseasesCode(ph.getDiseases_code()).getName());
//            patientsHistoryDto.setPatientName(patientsRepository.getByPatientsCode(ph.getPatients_code()).getName());
//            patientsHistoryDto.setCreateTime(ph.getCreate_time());
//            patientsHistoryDto.setDescription(ph.getDescription());
//            patientsHistoryDto.setId(ph.getId());
//            dto.add(patientsHistoryDto);
//        }
//        return dto;
//    }

    public boolean checkExist(Long id) {
        return patientsHistoryRepository.existsById(id);
    }

    public Object getListPage(int page, int sl) throws BadRequestException {
        return patientsHistoryRepository.findAll(PageRequest.of(page, sl));
    }

    public Object searchListHistory(Integer pageNumber, Integer pageSize,String doctorCode) {
        if(pageNumber <= 0) {
            return patientsHistoryRepository.findAll(new Sort(Sort.Direction.ASC, "id"));
        }
        if(pageSize <= 0) {
            pageSize = PAGE_SIZE;
        }

        List<String> lsDoctorCode=new ArrayList<>();

        if(doctorCode=="")
        {
            lsDoctorCode=patientsHistoryRepository.getAllDoctorCode();
        }
        else
        {
            lsDoctorCode.add(doctorCode);
        }

        PageRequest pageRequest =  PageRequest.of(pageNumber - 1, pageSize, Sort.Direction.ASC, "id");
        return patientsHistoryRepository.searchListDoctor(lsDoctorCode,pageRequest);
    }

    public Object getListHistory(Integer pageNumber, Integer pageSize) {
        if(pageNumber <= 0) {
            return patientsHistoryRepository.findAll(new Sort(Sort.Direction.ASC, "id"));
        }
        if(pageSize <= 0) {
            pageSize = PAGE_SIZE;
        }
        PageRequest pageRequest =  PageRequest.of(pageNumber - 1, pageSize, Sort.Direction.ASC, "id");

        return patientsHistoryRepository.findAll(pageRequest);
    }

    public Object getListHistoryDto(Integer pageNumber, Integer pageSize) {
        PageRequest pageRequest =  PageRequest.of(pageNumber - 1, pageSize, Sort.Direction.ASC, "id");
        Page<Patients_history> pagePatients = patientsHistoryRepository.findAll(pageRequest);
        List<PatientsHistoryDto> dto = null;
        PatientsHistoryDto content = new PatientsHistoryDto();
        for(Patients_history p: pagePatients.getContent()) {
            content.setDoctorName(doctorRepository.getByDoctorCode(p.getDoctor_code()).getName());
            content.setPatientName(patientsRepository.getByPatientsCode(p.getPatients_code()).getName());
            content.setDiseasesName(diseasesRepository.getByDiseasesCode(p.getDiseases_code()).getName());
            content.setCreateTime(p.getCreate_time());
            content.setDescription(p.getDescription());
            content.setId(p.getId());
            dto.add(content);
        }
        return dto;
    }
}
