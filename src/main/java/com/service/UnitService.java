package com.service;

import com.repository.UnitRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;

import com.exception.BadRequestException;
import com.entity.Unit;
import com.util.Utils;

@Service
public class UnitService extends AbstractService<Unit, Long>{
	@Autowired
    private UnitRepository unitRepository;

    @Override
    protected JpaRepository<Unit, Long> getRepository() {
        return unitRepository;
    }
    
    public Boolean checkExist(Long id) {
//        return unitRepository.exists(id);
    	return unitRepository.existsById(id);
    }
    public Object getListUnit(Integer pageNumber, Integer pageSize) {
        if(pageNumber <= 0) {
            return unitRepository.findAll(new Sort(Sort.Direction.ASC, "id"));
        }
        if(pageSize <= 0) {
            pageSize = PAGE_SIZE;
        }
        PageRequest pageRequest =  PageRequest.of(pageNumber - 1, pageSize, Sort.Direction.ASC, "id");
        return unitRepository.findAll(pageRequest);
    }

    public boolean checkDupUnit(String unit_name) {
        if (unitRepository.countUnitByName(unit_name) != 0) {
            return true;
        }
        return false;
    }
    
    public Unit save(Unit entity) throws BadRequestException {
        if(Utils.isEmptyText(entity.getUnit_name())) {
            throw new BadRequestException("Tên đơn vị không để trống");
        }
        Unit data = unitRepository.getByName(entity.getUnit_name().toLowerCase());
        if(data != null && !Utils.equal(data.getId(), entity.getId())) {
            throw new BadRequestException("Tên đơn vị đã tồn tại");
        }
        Unit data1 = unitRepository.getByUnitCode(entity.getUnit_code().toLowerCase());
        if(data1 != null && !Utils.equal(data1.getId(), entity.getId())) {
            throw new BadRequestException("Mã đã tồn tại");
        }
        
        return unitRepository.save(entity);
    }
    
    public void delete(long id) throws BadRequestException{
    	try {
    		unitRepository.deleteById(id);
    	}
    	catch(EmptyResultDataAccessException e){
    		 throw new ResourceNotFoundException("Đơn vị không tồn tại");
    	}
    }

}
