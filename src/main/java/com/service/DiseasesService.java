package com.service;

import com.entity.Diseases;
import com.exception.BadRequestException;
import com.repository.DiseasesRepository;
import com.util.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class DiseasesService extends AbstractService<Diseases, Long> {

    @Autowired
    private DiseasesRepository diseasesRepository;
    @Override
    protected JpaRepository<Diseases, Long> getRepository() {
        return diseasesRepository;
    }

    public boolean checkExist(Long id) {
        return diseasesRepository.existsById(id);
    }

    public boolean checkDupDiseases(String diseases_code) {
        if (diseasesRepository.countDiseasesByCode(diseases_code) != 0) {
            return true;
        }
        return false;
    }

    public Diseases save(Diseases entity) throws BadRequestException {
        if(Utils.isEmptyText(entity.getName())) {
            throw new BadRequestException("Tên bệnh không để trống");
        }
        Diseases data = diseasesRepository.getByDiseasesCode(entity.getDiseases_code().toLowerCase());
        if(data != null && !Utils.equal(data.getId(), entity.getId())) {
            throw new BadRequestException("Mã bệnh đã tồn tại");
        }
        return diseasesRepository.save(entity);
    }

    public Object getListPage(int page, int sl) throws BadRequestException {
        return diseasesRepository.findAll(PageRequest.of(page, sl));
    }

    public void delete(long id) throws BadRequestException{
        try {
            diseasesRepository.deleteById(id);
        }
        catch(EmptyResultDataAccessException e){
            throw new ResourceNotFoundException("Không có dữ liệu bệnh");
        }
    }

    public Object searchListDiseases(Integer pageNumber, Integer pageSize,String diseasesName) {
        if(pageNumber <= 0) {
            return diseasesRepository.findAll(new Sort(Sort.Direction.ASC, "id"));
        }
        if(pageSize <= 0) {
            pageSize = PAGE_SIZE;
        }

        List<String> lsDiseasesName=new ArrayList<>();

        if(diseasesName=="")
        {
            lsDiseasesName=diseasesRepository.getAllDiseasesName();
        }
        else
        {
            lsDiseasesName.add(diseasesName);
        }

        PageRequest pageRequest =  PageRequest.of(pageNumber - 1, pageSize, Sort.Direction.ASC, "id");
        return diseasesRepository.searchListDiseases(lsDiseasesName,pageRequest);
    }

    public Object getListDiseases(Integer pageNumber, Integer pageSize) {
        if(pageNumber <= 0) {
            return diseasesRepository.findAll(new Sort(Sort.Direction.ASC, "id"));
        }
        if(pageSize <= 0) {
            pageSize = PAGE_SIZE;
        }
        PageRequest pageRequest =  PageRequest.of(pageNumber - 1, pageSize, Sort.Direction.ASC, "id");
        return diseasesRepository.findAll(pageRequest);
    }
}
