package com.service;

import com.repository.RolesUsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

@Service
public class RolesUsersService {

	@Autowired
	private RolesUsersRepository rolesUsersRepository;

	public void delete(Long id) {
		try {
			rolesUsersRepository.deleteByUserId(id);
		} catch (EmptyResultDataAccessException e) {
		}
	}

}
