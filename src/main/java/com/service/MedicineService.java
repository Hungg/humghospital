package com.service;

import com.entity.Medicine;
import com.exception.BadRequestException;
import com.repository.MedicineRepository;
import com.util.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class MedicineService extends AbstractService<Medicine, Long> {
    @Autowired
    private MedicineRepository medicineRepository;
    @Override
    protected JpaRepository<Medicine, Long> getRepository() {
        return medicineRepository;
    }
    public boolean checkExist(Long id) {
        return medicineRepository.existsById(id);
    }

    public boolean checkDupMedicine(String medicine_code) {
        if (medicineRepository.countMedicineByCode(medicine_code) != 0) {
            return true;
        }
        return false;
    }

    public Medicine save(Medicine entity) throws BadRequestException {
        if(Utils.isEmptyText(entity.getName())) {
            throw new BadRequestException("Tên thuốc không để trống");
        }
        Medicine data = medicineRepository.getByMedicineCode(entity.getMedicine_code().toLowerCase());
        if(data != null && !Utils.equal(data.getId(), entity.getId())) {
            throw new BadRequestException("Mã thuốc đã tồn tại");
        }
        return medicineRepository.save(entity);
    }

    public Object getListPage(int page, int sl) throws BadRequestException {
        return medicineRepository.findAll(PageRequest.of(page, sl));
    }

    public void delete(long id) throws BadRequestException{
        try {
            medicineRepository.deleteById(id);
        }
        catch(EmptyResultDataAccessException e){
            throw new ResourceNotFoundException("Mã thuốc không tồn tại");
        }
    }

    public Object searchListMedicine(Integer pageNumber, Integer pageSize,String medicineCode) {
        if(pageNumber <= 0) {
            return medicineRepository.findAll(new Sort(Sort.Direction.ASC, "id"));
        }
        if(pageSize <= 0) {
            pageSize = PAGE_SIZE;
        }

        List<String> lsMedicineName=new ArrayList<>();

        if(medicineCode=="")
        {
            lsMedicineName=medicineRepository.getAllMedicineName();
        }
        else
        {
            lsMedicineName.add(medicineCode);
        }

        PageRequest pageRequest =  PageRequest.of(pageNumber - 1, pageSize, Sort.Direction.ASC, "id");
        return medicineRepository.searchListMedicine(lsMedicineName,pageRequest);
    }

    public Object getListMedicine(Integer pageNumber, Integer pageSize) {
        if(pageNumber <= 0) {
            return medicineRepository.findAll(new Sort(Sort.Direction.ASC, "id"));
        }
        if(pageSize <= 0) {
            pageSize = PAGE_SIZE;
        }
        PageRequest pageRequest =  PageRequest.of(pageNumber - 1, pageSize, Sort.Direction.ASC, "id");
        return medicineRepository.findAll(pageRequest);
    }

}
