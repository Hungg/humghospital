package com.service;

import com.entity.Doctor;
import com.exception.BadRequestException;
import com.repository.DoctorRepository;
import com.util.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class DoctorService extends AbstractService<Doctor, Long>{
    @Autowired
    private DoctorRepository doctorRepository;
    public boolean checkExist(Long id) {
        return doctorRepository.existsById(id);
    }

    @Override
    protected JpaRepository<Doctor, Long> getRepository() {
        return doctorRepository;
    }

    public boolean checkDupDoctor(String doctor_code) {
        if (doctorRepository.countDoctorByCode(doctor_code) != 0) {
            return true;
        }
        return false;
    }

    public Doctor save(Doctor entity) throws BadRequestException {
        if(Utils.isEmptyText(entity.getName())) {
            throw new BadRequestException("Tên bác sĩ không để trống");
        }
        Doctor data = doctorRepository.getByDoctorCode(entity.getDoctor_code().toLowerCase());
        if(data != null && !Utils.equal(data.getId(), entity.getId())) {
            throw new BadRequestException("Mã bác sĩ đã tồn tại");
        }
        return doctorRepository.save(entity);
    }

    public Object getListPage(int page, int sl) throws BadRequestException {
        return doctorRepository.findAll(PageRequest.of(page, sl));
    }

    public void delete(long id) throws BadRequestException{
        try {
            doctorRepository.deleteById(id);
        }
        catch(EmptyResultDataAccessException e){
            throw new ResourceNotFoundException("Bác sĩ không tồn tại");
        }
    }

    public Object searchListDoctor(Integer pageNumber, Integer pageSize,String doctorName) {
        if(pageNumber <= 0) {
            return doctorRepository.findAll(new Sort(Sort.Direction.ASC, "id"));
        }
        if(pageSize <= 0) {
            pageSize = PAGE_SIZE;
        }

        List<String> lsDoctorName=new ArrayList<>();

        if(doctorName=="")
        {
            lsDoctorName=doctorRepository.getAllDoctorName();
        }
        else
        {
            lsDoctorName.add(doctorName);
        }

        PageRequest pageRequest =  PageRequest.of(pageNumber - 1, pageSize, Sort.Direction.ASC, "id");
        return doctorRepository.searchListDoctor(lsDoctorName,pageRequest);
    }

    public Object getListDoctor(Integer pageNumber, Integer pageSize) {
        if(pageNumber <= 0) {
            return doctorRepository.findAll(new Sort(Sort.Direction.ASC, "id"));
        }
        if(pageSize <= 0) {
            pageSize = PAGE_SIZE;
        }
        PageRequest pageRequest =  PageRequest.of(pageNumber - 1, pageSize, Sort.Direction.ASC, "id");
        return doctorRepository.findAll(pageRequest);
    }
}
