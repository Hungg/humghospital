package com.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.repository.RoleRepository;
import com.repository.RolesUsersRepository;
import com.repository.UnitRepository;
import com.repository.UserRepository;
import com.model.PageDto;
import com.model.UsersUnitDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.entity.Role;
import com.entity.Unit;
import com.entity.User;


@Service
public class UsersService extends AbstractService<User, Long> {
    private static final int PAGE_SIZE = 10;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UnitRepository unitRepository;
   
    @Autowired
	private BCryptPasswordEncoder passwordEncoder;
    
    @Autowired
    private RolesUsersRepository rolesUsersRepository;
    
    @Autowired
    private RoleRepository roleRepository;

    @Override
    protected JpaRepository<User, Long> getRepository() {
        return userRepository;
    }
    
    public PageDto<UsersUnitDTO> getListUsers(Integer pageNumber, Integer pageSize) {
        if (pageSize <= 0) {
            pageSize = PAGE_SIZE;
        }
        
        PageRequest pageRequest = PageRequest.of(pageNumber - 1, pageSize, Sort.Direction.ASC, "id");

        // list user
        Page<User> usersPage = userRepository.findAll(pageRequest);
        List<User> usersEntity = usersPage.getContent();
        List<Long> listId = new ArrayList<>();
        List<UsersUnitDTO> listUserDto = new ArrayList<>();
        
        for (User entity : usersEntity) {
        	
            UsersUnitDTO dto = new UsersUnitDTO();
            dto.setId(entity.getId());
            dto.setUnit_id(entity.getUnit_id());
            dto.setUsername(entity.getUsername());
            dto.setEnabled(entity.getEnabled());
            dto.setEmail(entity.getEmail());
            dto.setName(entity.getFirstName()+" "+entity.getLastName());
            
            listUserDto.add(dto);
            listId.add(entity.getUnit_id());
        }


        List<Unit> listUnitEntity = unitRepository.findById(listId);
 

        for (Unit unit : listUnitEntity) {
            for (UsersUnitDTO usersUnitDTO : listUserDto) {
                if (!unit.getId().equals(usersUnitDTO.getUnit_id())) {
                    continue;
                }
                usersUnitDTO.setUnit_name(unit.getUnit_name());
            }
        }
        
        for (UsersUnitDTO usersUnitDTO : listUserDto) { 
        	
        	Long userId =usersUnitDTO.getId();
        	Long RoleId=rolesUsersRepository.getRoleIdByUser(userId);
        	
        	String nameRole=roleRepository.getRoleName(RoleId);
        	usersUnitDTO.setRole(nameRole);
        	
        }


        return new PageDto<>(usersPage, listUserDto);
    }
    
    public Object findGroupUser(Long id,Integer pageNumber, Integer pageSize) {
   	 if (pageSize <= 0) {
            pageSize = PAGE_SIZE;
        }
   	 	PageRequest pageRequest = PageRequest.of(pageNumber - 1, pageSize, Sort.Direction.ASC, "id");
        return userRepository.getByUnitId(id,pageRequest);
   }
    
    public User getUserByname(String name)
   	{
   		return userRepository.getByName(name);
   	}
    
    public User saveUser(User users, int status,String roles)
    {
    	
    	
    	User user = new User();
		user.setId(users.getId());
		user.setFirstName(users.getFirstName());
		user.setLastName(users.getLastName());
		user.setUsername(users.getUsername());
		
		
		
		// not md5
		if (status == 0) {
			
			User us = new User();
			us = userRepository.getById(users.getId());
			user.setPassword(us.getPassword());	
			user.setRoles(us.getRoles());
		}
		// md5
		if (status == 1) {
			user.setPassword(passwordEncoder.encode(users.getPassword()));
		    user.setRoles(Arrays.asList(new Role(roles)));

		}
		if(status==2)
		{
			User us = new User();
			us = userRepository.getById(users.getId());
			user.setPassword(passwordEncoder.encode(users.getPassword()));
		    user.setRoles(Arrays.asList(new Role(roles)));
		    user.setRoles(us.getRoles());
		}
		
		if(roles.equalsIgnoreCase("ROLE_ADMIN"))
		{
			user.setUnit_id((long) 0);
		}
		else
		{
			user.setUnit_id(users.getUnit_id());

		}
	
		user.setEmail(users.getEmail());
		user.setEnabled(users.getEnabled());
		
		return userRepository.save(user);
    }

}
