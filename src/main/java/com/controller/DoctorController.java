package com.controller;

import com.entity.Doctor;
import com.service.DoctorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
public class DoctorController {

    @Autowired
    private DoctorService doctorService;
    @RequestMapping(value = "/doctor/index")
    public String index() {
        return "doctor/index";
    }

    @GetMapping("/doctor")
    public ResponseEntity<Object> list(@RequestParam(value = "page", defaultValue = "0") int page,
                                       @RequestParam(value = "limit", defaultValue = "0") int limit) {
        return new ResponseEntity<Object>(doctorService.getListDoctor(page, limit), HttpStatus.OK);
    }

    @GetMapping("/doctor/search")
    public ResponseEntity<Object> search(@RequestParam(value = "page", defaultValue = "0") int page,
                                         @RequestParam(value = "limit", defaultValue = "0") int limit,@RequestParam(value="name") String name) {
        System.out.print(doctorService.searchListDoctor(page, limit,name ));
        return new ResponseEntity<Object>(doctorService.searchListDoctor(page, limit, name), HttpStatus.OK);
    }

    @RequestMapping(value = "/doctor", method = RequestMethod.POST, produces = {
            MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<Object> save(@RequestBody Doctor doctor) {
        return new ResponseEntity<Object>(doctorService.save(doctor), HttpStatus.OK);
    }

    @RequestMapping(value = "/doctor/{id}", method = RequestMethod.GET)
    public ResponseEntity<Object> get(@PathVariable Long id) {

        return new ResponseEntity<Object>(doctorService.get(id), HttpStatus.OK);
    }

    @RequestMapping(value="/doctor/{id}",method = RequestMethod.DELETE) public
    ResponseEntity<Object> delete(@PathVariable Long id){
        doctorService.delete(id);
        return new ResponseEntity<Object>(id,HttpStatus.OK);
    }

    @RequestMapping(value = "/doctor/list", method = RequestMethod.GET)
    public ResponseEntity<Object> getAll() {
        return new ResponseEntity<Object>(doctorService.fillAll(), HttpStatus.OK);
    }


}
