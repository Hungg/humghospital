package com.controller;

import com.entity.Diseases;
import com.service.DiseasesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
public class DiseasesController {
    @Autowired
    private DiseasesService diseasesService;
    @RequestMapping(value = "/diseases/index")
    public String index() {
        return "diseases/index";
    }

    @GetMapping("/diseases")
    public ResponseEntity<Object> list(@RequestParam(value = "page", defaultValue = "0") int page,
                                       @RequestParam(value = "limit", defaultValue = "0") int limit) {
        return new ResponseEntity<Object>(diseasesService.getListDiseases(page, limit), HttpStatus.OK);
    }

    @GetMapping("/diseases/search")
    public ResponseEntity<Object> search(@RequestParam(value = "page", defaultValue = "0") int page,
                                         @RequestParam(value = "limit", defaultValue = "0") int limit,@RequestParam(value="name") String name) {
        System.out.print(diseasesService.searchListDiseases(page, limit,name ));
        return new ResponseEntity<Object>(diseasesService.searchListDiseases(page, limit, name), HttpStatus.OK);
    }

    @RequestMapping(value = "/diseases", method = RequestMethod.POST, produces = {
            MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<Object> save(@RequestBody Diseases diseases) {
        return new ResponseEntity<Object>(diseasesService.save(diseases), HttpStatus.OK);
    }

    @RequestMapping(value = "/diseases/{id}", method = RequestMethod.GET)
    public ResponseEntity<Object> get(@PathVariable Long id) {

        return new ResponseEntity<Object>(diseasesService.get(id), HttpStatus.OK);
    }

    @RequestMapping(value="/diseases/{id}",method = RequestMethod.DELETE) public
    ResponseEntity<Object> delete(@PathVariable Long id){
        diseasesService.delete(id);
        return new ResponseEntity<Object>(id,HttpStatus.OK);
    }

    @RequestMapping(value = "/diseases/list", method = RequestMethod.GET)
    public ResponseEntity<Object> getAll() {
        return new ResponseEntity<Object>(diseasesService.fillAll(), HttpStatus.OK);
    }

}
