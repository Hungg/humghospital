package com.controller;

import com.service.PatientsHistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;


@Controller
@PreAuthorize("hasRole('ROLE_ADMIN')")
public class PatientsHistoryController {
    @Autowired
    private PatientsHistoryService patientsHistoryService;
    @RequestMapping(value = "/patients/index")
    public String index() {
        return "patients/index";
    }

    @GetMapping("/patients")
    public ResponseEntity<Object> list(@RequestParam(value = "page", defaultValue = "0") int page,
                                       @RequestParam(value = "limit", defaultValue = "0") int limit) {
        return new ResponseEntity<Object>(patientsHistoryService.getListHistory(page, limit), HttpStatus.OK);
    }

    @GetMapping("/patients_history/search")
    public ResponseEntity<Object> search(@RequestParam(value = "page", defaultValue = "0") int page,
                                         @RequestParam(value = "limit", defaultValue = "0") int limit,
                                         @RequestParam(value="name") String name) {
//        System.out.print(doctorService.searchListDoctor(page, limit,name ));
        return new ResponseEntity<Object>(patientsHistoryService.searchListHistory(page, limit, name), HttpStatus.OK);
    }

    @RequestMapping(value = "/patients/{id}", method = RequestMethod.GET)
    public ResponseEntity<Object> get(@PathVariable Long id) {

        return new ResponseEntity<Object>(patientsHistoryService.get(id), HttpStatus.OK);
    }

    @RequestMapping(value = "/patients/list", method = RequestMethod.GET)
    public ResponseEntity<Object> getAll() {
        return new ResponseEntity<Object>(patientsHistoryService.fillAll(), HttpStatus.OK);
    }

}
