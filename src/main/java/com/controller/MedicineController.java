package com.controller;

import com.entity.Medicine;
import com.service.MedicineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
public class MedicineController {
    @Autowired
    private MedicineService medicineService;
    @RequestMapping(value = "/medicine/index")
    public String index() {
        return "medicine/index";
    }

    @GetMapping("/medicine")
    public ResponseEntity<Object> list(@RequestParam(value = "page", defaultValue = "0") int page,
                                       @RequestParam(value = "limit", defaultValue = "0") int limit) {
        return new ResponseEntity<Object>(medicineService.getListMedicine(page, limit), HttpStatus.OK);
    }

    @GetMapping("/medicine/search")
    public ResponseEntity<Object> search(@RequestParam(value = "page", defaultValue = "0") int page,
                                         @RequestParam(value = "limit", defaultValue = "0") int limit,@RequestParam(value="name") String name) {
        System.out.print(medicineService.searchListMedicine(page, limit,name ));
        return new ResponseEntity<Object>(medicineService.searchListMedicine(page, limit, name), HttpStatus.OK);
    }

    @RequestMapping(value = "/medicine", method = RequestMethod.POST, produces = {
            MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<Object> save(@RequestBody Medicine medicine) {
        return new ResponseEntity<Object>(medicineService.save(medicine), HttpStatus.OK);
    }

    @RequestMapping(value = "/medicine/{id}", method = RequestMethod.GET)
    public ResponseEntity<Object> get(@PathVariable Long id) {

        return new ResponseEntity<Object>(medicineService.get(id), HttpStatus.OK);
    }

    @RequestMapping(value="/medicine/{id}",method = RequestMethod.DELETE) public
    ResponseEntity<Object> delete(@PathVariable Long id){
        medicineService.delete(id);
        return new ResponseEntity<Object>(id,HttpStatus.OK);
    }

    @RequestMapping(value = "/medicine/list", method = RequestMethod.GET)
    public ResponseEntity<Object> getAll() {
        return new ResponseEntity<Object>(medicineService.fillAll(), HttpStatus.OK);
    }

}
