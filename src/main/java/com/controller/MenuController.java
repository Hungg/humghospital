package com.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class MenuController {
	 
    @RequestMapping("/")
    public String index() {
      return "redirect:/dashboard/index";
    } 
}
