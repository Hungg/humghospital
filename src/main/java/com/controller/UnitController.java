package com.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.entity.Unit;
import com.service.UnitService;

@Controller
@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
public class UnitController {
	private UnitService unitService;
	@Autowired
	public void setUnitService(UnitService unitService) {
		this.unitService = unitService;
	}

	
	@RequestMapping(value = "/units/index")
	public String index() {
		return "units/index";
	}

	@GetMapping("/units")
	public ResponseEntity<Object> list(@RequestParam(value = "page", defaultValue = "0") int page,
			@RequestParam(value = "limit", defaultValue = "0") int limit) {
		return new ResponseEntity<Object>(unitService.getListUnit(page, limit), HttpStatus.OK);
	}

	@RequestMapping(value = "/units", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<Object> save(@RequestBody Unit unit) {
		return new ResponseEntity<Object>(unitService.save(unit), HttpStatus.OK);
	}

	@RequestMapping(value = "/units/{id}", method = RequestMethod.GET)
	public ResponseEntity<Object> get(@PathVariable Long id) {
		
		return new ResponseEntity<Object>(unitService.get(id), HttpStatus.OK);
	}
		
	  @RequestMapping(value="/unit/{id}",method = RequestMethod.DELETE) public
	  ResponseEntity<Object> delete(@PathVariable Long id){
	  unitService.delete(id);
	  return new ResponseEntity<Object>(id,HttpStatus.OK);
	  }
	  
	  @RequestMapping(value = "/units/list", method = RequestMethod.GET)
		public ResponseEntity<Object> getAll() {
			return new ResponseEntity<Object>(unitService.fillAll(), HttpStatus.OK);
		}
	 


}
