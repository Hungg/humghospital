package com.controller;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.entity.User;
import com.service.RoleService;
import com.service.RolesUsersService;
import com.service.UsersService;

@Controller

public class UserController {

	@Autowired
	private UsersService usersService;
	
	@Autowired
	private RolesUsersService rolesUsersService;

	@Autowired
	private RoleService roleService;
	

	@RequestMapping(value = "/users/index")
	public String list() {
		return "users/list";
	}

	@GetMapping("/users")
	public ResponseEntity<Object> list(@RequestParam(value = "page", defaultValue = "0") int page,
			@RequestParam(value = "limit", defaultValue = "0") int limit) {
		return new ResponseEntity<Object>(usersService.getListUsers(page, limit), HttpStatus.OK);
	}

	@RequestMapping(value = "/user/userinfo/", method = RequestMethod.GET)
	public ResponseEntity<Object> getName(@RequestParam(value = "username") String username) {
		return new ResponseEntity<Object>(usersService.getUserByname(username), HttpStatus.OK);
	}

	@RequestMapping(value = "/users/delete/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Object> delete(@PathVariable Long id) {
		rolesUsersService.delete(id);
		usersService.delete(id);
		roleService.delete(id);
		return new ResponseEntity<Object>("success", HttpStatus.OK);
	}

	@RequestMapping(value = "/users/{id}", method = RequestMethod.GET)
	public ResponseEntity<Object> get(@PathVariable Long id) {
		return new ResponseEntity<Object>(usersService.get(id), HttpStatus.OK);
	}

	@RequestMapping(value = "/users/group", method = RequestMethod.GET)
	public ResponseEntity<Object> getGroup(@RequestParam(value = "page", defaultValue = "1") int page,
			@RequestParam(value = "limit", defaultValue = "0") int limit,
			@RequestParam(value = "unit_id") Long unit_id) {
		return new ResponseEntity<Object>(usersService.findGroupUser(unit_id,page,limit), HttpStatus.OK);
	}

	@RequestMapping(value = "/users/list", method = RequestMethod.GET)
	public ResponseEntity<Object> getAll() {
		return new ResponseEntity<Object>(usersService.fillAll(), HttpStatus.OK);
	}

	@RequestMapping(value = "/user", method = RequestMethod.POST)
	public ResponseEntity<Object> save(@RequestBody User users, int status,String roles) {
		return new ResponseEntity<Object>(usersService.saveUser(users,status,roles), HttpStatus.OK);
	}

	@RequestMapping(value = "/user/role", method = RequestMethod.GET)
	public Collection<? extends GrantedAuthority> getRoleUser() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		return auth.getAuthorities();
	}

	// mã hóa md5
	public static String md5(String str) {
		String result = "";
		MessageDigest digest;
		try {
			digest = MessageDigest.getInstance("MD5");
			digest.update(str.getBytes());
			BigInteger bigInteger = new BigInteger(1, digest.digest());
			result = bigInteger.toString(16);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return result;
	}

}
