package com.model;

import java.util.Date;

import com.entity.AbstractModel;

public class NewsReceiverUnitDTO extends AbstractModel<Long>{
	private Long id;
	private Long news_id;
	private String from_unit_code;
	private String to_unit_code;
	private String name_unit_send;
	private String name_unit_take;
	private Date creation_date;
	private Date received_date;
	private String status;
	
	
	public NewsReceiverUnitDTO() {
		super();
		// TODO Auto-generated constructor stub
	}


	public NewsReceiverUnitDTO(Long id, Long news_id, String from_unit_code, String to_unit_code, String name_unit_send,
			String name_unit_take, Date creation_date, Date received_date, String status) {
		super();
		this.id = id;
		this.news_id = news_id;
		this.from_unit_code = from_unit_code;
		this.to_unit_code = to_unit_code;
		this.name_unit_send = name_unit_send;
		this.name_unit_take = name_unit_take;
		this.creation_date = creation_date;
		this.received_date = received_date;
		this.status = status;
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public Long getNews_id() {
		return news_id;
	}


	public void setNews_id(Long news_id) {
		this.news_id = news_id;
	}


	public String getFrom_unit_code() {
		return from_unit_code;
	}


	public void setFrom_unit_code(String from_unit_code) {
		this.from_unit_code = from_unit_code;
	}


	public String getTo_unit_code() {
		return to_unit_code;
	}


	public void setTo_unit_code(String to_unit_code) {
		this.to_unit_code = to_unit_code;
	}


	public String getName_unit_send() {
		return name_unit_send;
	}


	public void setName_unit_send(String name_unit_send) {
		this.name_unit_send = name_unit_send;
	}


	public String getName_unit_take() {
		return name_unit_take;
	}


	public void setName_unit_take(String name_unit_take) {
		this.name_unit_take = name_unit_take;
	}


	public Date getCreation_date() {
		return creation_date;
	}


	public void setCreation_date(Date creation_date) {
		this.creation_date = creation_date;
	}


	public Date getReceived_date() {
		return received_date;
	}


	public void setReceived_date(Date received_date) {
		this.received_date = received_date;
	}


	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}
	
	
	
	

}
