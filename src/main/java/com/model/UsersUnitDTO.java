package com.model;

import com.entity.AbstractModel;

public class UsersUnitDTO extends AbstractModel<Long> {
    private Long id;
    private String username;
    private Long unit_id;
    private Integer enabled;
    private String email;
    private String unit_name;
    private String name;
    private String role;

    public UsersUnitDTO(){

    }

    public UsersUnitDTO(Long id, String username, String unit_name, Integer enabled, String email, Long unit_id,String name,String role){
        this.id = id;
        this.username = username;
        this.unit_name = unit_name;
        this.enabled = enabled;
        this.email = email;
        this.unit_id = unit_id;
        this.name=name;
        this.role=role;
    }

    public Long getId() {
        return id;
    }

    public String getUnit_name() {
        return unit_name;
    }

    public Integer getEnabled() {
        return enabled;
    }

    public String getEmail() {
        return email;
    }

    public Long getUnit_id() {
        return unit_id;
    }



    public String getUsername() {
        return username;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setUnit_name(String unit_name) {
        this.unit_name = unit_name;
    }

    public void setUnit_id(Long unit_id) {
        this.unit_id = unit_id;
    }

    public void setEnabled(Integer enabled) {
        this.enabled = enabled;
    }

    public void setEmail(String email) {
        this.email = email;
    }


    public void setUsername(String username) {
        this.username = username;
    }
    
    
    public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	@Override
    public String toString() {
        return "UsersUnitDTO [id=" + id + ", username=" + username + ", enabled=" + enabled + ", email=" + email + ", unitName=" + unit_name + "unitId" + unit_id + "]";
    }

}
