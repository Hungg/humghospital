package com.model;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.domain.Page;

import com.util.Utils;

public class PageDto<T extends Serializable> implements Serializable {
    private static final long serialVersionUID = 1L;
    private boolean first;
    private boolean last;
    private long number;
    private long numberOfElements;
    private long size;
    private long totalElements;
    private long totalPages;
    private List<?> content;

    @SuppressWarnings("rawtypes")
    public PageDto(Page page, List<T> content) {
        this.content = content;
        this.first = page.isFirst();
        this.last = page.isLast();
        this.number = page.getNumber();
        this.numberOfElements = page.getNumberOfElements();
        this.size = page.getSize();
        this.totalElements = page.getTotalElements();
        this.totalPages = page.getTotalPages();
    }
    public boolean isFirst() {
        return first;
    }

    public boolean isLast() {
        return last;
    }

    public long getNumber() {
        return number;
    }

    public long getNumberOfElements() {
        return numberOfElements;
    }

    public long getSize() {
        return size;
    }

    public long getTotalElements() {
        return totalElements;
    }

    public long getTotalPages() {
        return totalPages;
    }

    public List<?> getContent() {
        return content;
    }
}
