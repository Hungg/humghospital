package com.model;

public class ExcelDataPostDto {
    private DataPostDto data;
    private int rowIndex;
    private String message;
    private boolean success;

    public DataPostDto getData() {
        return data;
    }

    public void setData(DataPostDto data) {
        this.data = data;
    }

    public int getRowIndex() {
        return rowIndex;
    }

    public void setRowIndex(int rowIndex) {
        this.rowIndex = rowIndex;
    }

    public String getMessage() {
        return message;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setStatus(boolean success, String message) {
        this.success = success;
        this.message = message;
    }

}
