package com.model;

import java.io.Serializable;

public class FileDownloadDto implements Serializable {
    private static final long serialVersionUID = 1L;

    private String name;
    private String src;

    public FileDownloadDto(String name, String src) {
        this.name = name;
        this.src = src;
    }

    public String getName() {
        return name;
    }

    public String getSrc() {
        return src;
    }
}
