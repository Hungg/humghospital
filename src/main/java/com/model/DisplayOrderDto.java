package com.model;

import java.io.Serializable;

public class DisplayOrderDto implements Serializable {

    private static final long serialVersionUID = 1L;
    
    private Long id;
    private Integer displayOrder;

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public Integer getDisplayOrder() {
        return displayOrder;
    }
    public void setDisplayOrder(Integer displayOrder) {
        this.displayOrder = displayOrder;
    }

}
