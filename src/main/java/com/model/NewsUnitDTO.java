package com.model;

import java.sql.Blob;
import java.util.Base64;
import java.util.Date;

import com.entity.AbstractModel;

public class NewsUnitDTO extends AbstractModel<Long>{
	
	private Long id;
	private String from_unit_code;
	private String name_unit;
	private String type;
	private Date creation_date;
	private Blob data;
	
	public NewsUnitDTO() {
	}
	public NewsUnitDTO(Long id, String from_unit_code, String name_unit, String type, Date creation_date, Blob data) {
		this.id = id;
		this.from_unit_code = from_unit_code;
		this.name_unit = name_unit;
		this.type = type;
		this.creation_date = creation_date;
		this.data = data;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getFrom_unit_code() {
		return from_unit_code;
	}
	public void setFrom_unit_code(String from_unit_code) {
		this.from_unit_code = from_unit_code;
	}
	public String getName_unit() {
		return name_unit;
	}
	public void setName_unit(String name_unit) {
		this.name_unit = name_unit;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Date getCreation_date() {
		return creation_date;
	}
	public void setCreation_date(Date creation_date) {
		this.creation_date = creation_date;
	}
	public String getData() {
		// Getting decoder  
        Base64.Decoder decoder = Base64.getDecoder();  
        // Decoding string  
        String dStr = "";
		try {
			if (this.data != null && this.data.length() > 0) {
				dStr = new String(decoder.decode(this.data.getBytes(1, (int) this.data.length())));
			} else {
				return "";
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return dStr;
	}
	public void setData(Blob data) {
		this.data = data;
	}
	
	

}
