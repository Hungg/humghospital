package com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.web.context.request.RequestContextListener;

@SpringBootApplication
//bật tính năng Schedule cho Spring
@EnableScheduling
public class Application {

	@Bean
	public RequestContextListener requestContextListener() {
		return new RequestContextListener();
	}
	
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
	
	// thiết lập các thông số cho tính năng schedule
		@Bean
		public TaskScheduler taskScheduler() {
			final ThreadPoolTaskScheduler scheduler = new ThreadPoolTaskScheduler();
			scheduler.setPoolSize(10);
			return scheduler;
		}
	
}

