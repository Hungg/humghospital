package com.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
@Table(name = "doctor_patients")
public class Doctor_patients implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, unique = true)
    private Long id;

    @Column(name = "doctor_code", nullable = false)
    private String doctor_code;

    @Column(name = "patients_code", nullable = false)
    private String patients_code;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDoctor_code() {
        return doctor_code;
    }

    public void setDoctor_code(String doctor_code) {
        this.doctor_code = doctor_code;
    }

    public String getPatients_code() {
        return patients_code;
    }

    public void setPatients_code(String patients_code) {
        this.patients_code = patients_code;
    }
}
