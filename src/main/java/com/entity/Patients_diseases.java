package com.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table (name = "patients_diseases")
public class Patients_diseases implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, unique = true)
    private Long id;

    @Column(name = "diseases_code", nullable = false)
    private String diseases_code;

    @Column(name = "patients_code", nullable = false)
    private String patients_code;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDiseases_code() {
        return diseases_code;
    }

    public void setDiseases_code(String diseases_code) {
        this.diseases_code = diseases_code;
    }

    public String getPatients_code() {
        return patients_code;
    }

    public void setPatients_code(String patients_code) {
        this.patients_code = patients_code;
    }
}
