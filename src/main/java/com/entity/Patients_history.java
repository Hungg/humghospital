package com.entity;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@Table(name = "patients_history")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Patients_history extends AbstractModel<Long> {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, unique = true)
    private Long id;

    @Column(name = "doctor_code", nullable = false)
    private String doctor_code;

    @Column(name = "patients_code", nullable = false)
    private String patients_code;

    @Column(name = "diseases_code", nullable = false)
    private String diseases_code;

    @Column(name = "create_time", columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable = false, updatable = false)
    private Date create_time;

    @Column(name = "description")
    private String description;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDoctor_code() {
        return doctor_code;
    }

    public void setDoctor_code(String doctor_code) {
        this.doctor_code = doctor_code;
    }

    public String getPatients_code() {
        return patients_code;
    }

    public void setPatients_code(String patients_code) {
        this.patients_code = patients_code;
    }

    public String getDiseases_code() {
        return diseases_code;
    }

    public void setDiseases_code(String diseases_code) {
        this.diseases_code = diseases_code;
    }

    public Date getCreate_time() {
        return create_time;
    }

    public void setCreate_time(Date create_time) {
        this.create_time = create_time;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
