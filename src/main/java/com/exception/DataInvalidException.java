package com.exception;

import com.model.DataFieldErrorDto;
import org.springframework.web.bind.annotation.ResponseStatus;

import org.springframework.http.HttpStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class DataInvalidException extends RuntimeException {
    private static final long serialVersionUID = 1;

    private DataFieldErrorDto error;

    public DataInvalidException(DataFieldErrorDto error) {
        super(error.getMessage());
    }

    public DataFieldErrorDto getError() {
        return error;
    }

    public void setError(DataFieldErrorDto error) {
        this.error = error;
    }

}
