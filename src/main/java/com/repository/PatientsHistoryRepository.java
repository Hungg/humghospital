package com.repository;

import com.entity.Doctor;
import com.entity.Patients_history;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface PatientsHistoryRepository extends JpaRepository<Patients_history, Long> {
//    @Query(value = "SELECT * FROM  patients_history WHERE (CREATE_TIME BETWEEN :fromDate AND :toDate)",
//            countQuery = "SELECT COUNT(*) FROM  patients_history WHERE (CREATE_TIME BETWEEN :fromDate AND :toDate)",
//            nativeQuery = true)
//    Page<Patients_history> getList(@Param("fromDate") @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") Date fromDate, @Param("toDate") @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") Date toDate, Pageable pageable);

    @Query(value = "SELECT count(a.id) FROM Patients_history a WHERE a.id = ?1 LIMIT 1", nativeQuery = true)
    int countHistoryById(String id);

    @Query("SELECT c FROM Patients_history c WHERE c.patients_code = ?1")
    Patients_history getByPatientsCode(String patients_code);

    @Query("SELECT c FROM Patients_history c WHERE c.doctor_code = ?1")
    Patients_history getByDoctorCode(String doctor_code);

    @Query("SELECT c FROM Patients_history c WHERE c.doctor_code IN (?1) ORDER BY c.doctor_code ASC, c.id DESC")
    List<Patients_history> findByDoctorCode(List<String> doctor_code);

    @Query("SELECT c FROM Patients_history c WHERE c.patients_code IN (?1) ORDER BY c.patients_code ASC, c.id DESC")
    List<Patients_history> findByPatientsCode(List<String> patients_code);

    @Query("SELECT u.doctor_code FROM Patients_history u")
    List<String> getAllDoctorCode();

    @Query("SELECT u.patients_code FROM Patients_history u")
    List<String> getAllPatientsCode();

    @Query(value="SELECT * FROM patients_history d WHERE d.doctor_code IN :doctor_code",
            countQuery="SELECT * FROM patients_history d WHERE d.doctor_code IN :doctor_code",
            nativeQuery = true)
    Page<Patients_history> searchListDoctor(@Param("doctor_code") List<String> doctor_code, Pageable pageable);


}
