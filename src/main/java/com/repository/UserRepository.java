package com.repository;



import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.entity.User;


@Repository
public interface UserRepository extends JpaRepository<User, Long> {
	User findByEmail(String email);
	
	 @Query("SELECT c FROM User c WHERE c.username = ?1")
	User findByUserName(String username);
	
	 @Query("SELECT c FROM User c WHERE c.email = ?1")
	    User getByName(String name);
	 
	 @Query("SELECT c FROM User c WHERE c.id = ?1")
	    User getById(Long id);
	 
	 @Query(value="SELECT * FROM user WHERE  unit_id=:unit_id",
				countQuery="SELECT COUNT(*) FROM user WHERE  unit_id=:unit_id",
			 nativeQuery = true)
		  Page<User> getByUnitId(@Param("unit_id") Long unit_id, Pageable pageable);
}
