package com.repository;

import com.entity.Diseases;
import com.entity.Patients;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PatientsRepository extends JpaRepository<Patients, Long> {
    @Query(value = "SELECT count(a.patients_code) FROM Patients a WHERE a.patients_code = ?1 LIMIT 1", nativeQuery = true)
    int countPatientsByCode(String patients_code);

    @Query("SELECT c FROM Patients c WHERE c.id IN (?1) ORDER BY c.patients_code ASC, c.id DESC")
    List<Patients> findById(List<Long> patients_code);

    @Query("SELECT c FROM Patients c WHERE c.name = ?1")
    Patients getByName(String name);

    @Query("SELECT c FROM Patients c WHERE c.patients_code = ?1")
    Patients getByPatientsCode(String patients_code);

    @Query("SELECT c FROM Patients c WHERE c.patients_code IN (?1) ORDER BY c.patients_code ASC, c.id DESC")
    List<Patients> findByPatientsCode(List<String> patients_code);
}
