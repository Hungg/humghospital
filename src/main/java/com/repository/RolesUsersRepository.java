package com.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.entity.RolesUsers;

@Repository
@Transactional
public interface RolesUsersRepository extends JpaRepository<RolesUsers, Long>{

	@Modifying
	@Query("delete from RolesUsers where user_id = :user_id")
	void deleteByUserId(@Param("user_id") Long user_id);
	
	@Query(value="SELECT r.role_id FROM RolesUsers r WHERE r.user_id=:user_id")
	Long getRoleIdByUser(@Param("user_id") Long user_id);

}
