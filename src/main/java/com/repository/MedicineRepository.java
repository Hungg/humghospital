package com.repository;


import com.entity.Medicine;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MedicineRepository extends JpaRepository<Medicine, Long> {
    @Query(value = "SELECT count(a.medicine_code) FROM Medicine a WHERE a.medicine_code = ?1 LIMIT 1", nativeQuery = true)
    int countMedicineByCode(String medicine_code);
    @Query("SELECT c FROM Medicine c WHERE c.id IN (?1) ORDER BY c.medicine_code ASC, c.id DESC")
    List<Medicine> findById(List<Long> medicine_id);

    @Query("SELECT c FROM Medicine c WHERE c.name = ?1")
    Medicine getByName(String name);

    @Query("SELECT c FROM Medicine c WHERE c.medicine_code = ?1")
    Medicine getByMedicineCode(String medicine_code);

    @Query("SELECT c FROM Medicine c WHERE c.medicine_code IN (?1) ORDER BY c.medicine_code ASC, c.id DESC")
    List<Medicine> findByMedicineCode(List<String> medicine_code);

    @Query("SELECT u.name FROM Medicine u")
    List<String> getAllMedicineName();

    @Query(value="SELECT * FROM medicine d WHERE d.name IN :name",
            countQuery="SELECT * FROM medicine d WHERE d.name IN :name",
            nativeQuery = true)
    Page<Medicine> searchListMedicine(@Param("name") List<String> medicine_name, Pageable pageable);

}
