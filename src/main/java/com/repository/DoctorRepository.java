package com.repository;

import com.entity.Doctor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DoctorRepository extends JpaRepository<Doctor, Long> {
    @Query(value = "SELECT count(a.doctor_code) FROM Doctor a WHERE a.doctor_code = ?1 LIMIT 1", nativeQuery = true)
    int countDoctorByCode(String doctor_code);

    @Query("SELECT c FROM Doctor c WHERE c.doctor_code = ?1")
    Doctor getByDoctorCode(String doctor_code);

    @Query("SELECT c FROM Doctor c WHERE c.name = ?1")
    Doctor getByDoctorName(String doctor_name);

    @Query("SELECT c FROM Doctor c WHERE c.doctor_code IN (?1) ORDER BY c.doctor_code ASC, c.id DESC")
    List<Doctor> findByDoctorcode(List<String> doctor_code);

    @Query("SELECT u.doctor_code FROM Doctor u")
    List<String> getAllDoctorCode();

    @Query("SELECT u.name FROM Doctor u")
    List<String> getAllDoctorName();

    @Query(value="SELECT * FROM doctor d WHERE d.name IN :name",
            countQuery="SELECT * FROM doctor d WHERE d.name IN :name",
            nativeQuery = true)
    Page<Doctor> searchListDoctor(@Param("name") List<String> doctor_name, Pageable pageable);

}
