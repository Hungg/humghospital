package com.repository;

import com.entity.Diseases;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DiseasesRepository extends JpaRepository<Diseases, Long> {
    @Query(value = "SELECT count(a.diseases_code) FROM Diseases a WHERE a.diseases_code = ?1 LIMIT 1", nativeQuery = true)
    int countDiseasesByCode(String diseases_code);
    @Query("SELECT c FROM Diseases c WHERE c.id IN (?1) ORDER BY c.diseases_code ASC, c.id DESC")
    List<Diseases> findById(List<Long> diseases_id);

    @Query("SELECT c FROM Medicine c WHERE c.name = ?1")
    Diseases getByName(String name);

    @Query("SELECT c FROM Diseases c WHERE c.diseases_code = ?1")
    Diseases getByDiseasesCode(String diseases_code);

    @Query("SELECT c FROM Diseases c WHERE c.diseases_code IN (?1) ORDER BY c.diseases_code ASC, c.id DESC")
    List<Diseases> findByDiseasesCode(List<String> diseases_code);

    @Query("SELECT u.name FROM Diseases u")
    List<String> getAllDiseasesName();

    @Query(value="SELECT * FROM diseases d WHERE d.name IN :name",
            countQuery="SELECT * FROM diseases d WHERE d.name IN :name",
            nativeQuery = true)
    Page<Diseases> searchListDiseases(@Param("name") List<String> diseases_name, Pageable pageable);
}
