package com.repository.basic;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import com.model.DataPostDto;



public interface DataPostRepositoryBasic {

    Page<DataPostDto> findByCategory(Long cateId, PageRequest pageRequest);

    List<DataPostDto> findByCategory(Long cateId);

    Page<DataPostDto> findByCategories(List<Long> listCateId, PageRequest pageRequest);

    Page<DataPostDto> findAll(PageRequest pageRequest);
}
