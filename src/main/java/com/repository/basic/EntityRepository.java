package com.repository.basic;

import java.math.BigInteger;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;


public abstract class EntityRepository {
    @PersistenceContext
    EntityManager entityManager;

    protected EntityManager getEntityManager() {
        return entityManager;
    }

    @SuppressWarnings("unchecked")
    protected <T> Page<T> fetchPaging(String sqlQuery, String sqlCountQuery, Map<String, Object> params, Class<T> clazz, PageRequest pageRequest) {
        Query query = entityManager.createNativeQuery(sqlQuery, clazz);
        query.setMaxResults(pageRequest.getPageSize());
        query.setFirstResult(pageRequest.getPageNumber()*pageRequest.getPageSize());
        Query countQuery = entityManager.createNativeQuery(sqlCountQuery);
        if(params != null) {
            for(Map.Entry<String, Object> entry : params.entrySet()) {
                query.setParameter(entry.getKey(), entry.getValue());
                countQuery.setParameter(entry.getKey(), entry.getValue());
            }
        }
        BigInteger total = (BigInteger)countQuery.getSingleResult();
        PageImpl<T> pageImp = new PageImpl<T>(query.getResultList(), pageRequest, total.longValue());
        return pageImp;
    }

    @SuppressWarnings("unchecked")
    protected Page<Object[]> fetchPaging(String sqlQuery, String sqlCountQuery, Map<String, Object> params, PageRequest pageRequest) {
        Query query = entityManager.createNativeQuery(sqlQuery);
        query.setMaxResults(pageRequest.getPageSize());
        query.setFirstResult(pageRequest.getPageNumber()*pageRequest.getPageSize());
        Query countQuery = entityManager.createNativeQuery(sqlCountQuery);
        if(params != null) {
            for(Map.Entry<String, Object> entry : params.entrySet()) {
                query.setParameter(entry.getKey(), entry.getValue());
                countQuery.setParameter(entry.getKey(), entry.getValue());
            }
        }
        BigInteger total = (BigInteger)countQuery.getSingleResult();
        PageImpl<Object[]> pageImp = new PageImpl<Object[]>(query.getResultList(), pageRequest, total.longValue());
        return pageImp;
    }

    @SuppressWarnings("unchecked")
    protected List<Object[]> fetchList(String sqlQuery, Map<String, Object> params) {
        Query query = entityManager.createNativeQuery(sqlQuery);
        if(params != null) {
            for(Map.Entry<String, Object> entry : params.entrySet()) {
                query.setParameter(entry.getKey(), entry.getValue());
            }
        }
        return query.getResultList();
    }
}
