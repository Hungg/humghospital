package com.repository.basic;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Repository;

import com.model.DataPostDto;



@Repository
public class DataPostRepositoryImpl extends EntityRepository implements DataPostRepositoryBasic {

    @Override
    public Page<DataPostDto> findByCategories(List<Long> listCateId, PageRequest pageRequest) {
        String sqlQuery = "SELECT p.id, p.cate_id, c.name"
                +" FROM data_post p LEFT JOIN data_category c ON p.cate_id = c.id"
                +" WHERE p.cate_id IN (:cateid)"
                +" ORDER BY p.created_date desc";
        String sqlCountQuery = "SELECT count(p.id) FROM data_post p WHERE p.cate_id IN (:cateid)";
        HashMap<String, Object> params = new HashMap<>();
        params.put("cateid", listCateId);
        Page<Object[]> page = fetchPaging(sqlQuery, sqlCountQuery, params, pageRequest);
        ArrayList<DataPostDto> data = map(page.getContent());
        return new PageImpl<DataPostDto>(data, pageRequest, page.getTotalElements());
    }

    @Override
    public Page<DataPostDto> findAll(PageRequest pageRequest) {
        String sqlQuery = "SELECT p.id, p.cate_id, c.name"
                +" FROM data_post p LEFT JOIN data_category c ON p.cate_id = c.id"
                +" ORDER BY p.created_time desc";
        String sqlCountQuery = "SELECT count(p.id) FROM data_post p";
        Page<Object[]> page = fetchPaging(sqlQuery, sqlCountQuery, null, pageRequest);
        ArrayList<DataPostDto> data = map(page.getContent());
        return new PageImpl<DataPostDto>(data, pageRequest, page.getTotalElements());
    }

    private ArrayList<DataPostDto> map(List<Object[]> content) {
        ArrayList<DataPostDto> data = new ArrayList<>();
        if(content == null) {
            return data;
        }
        for(Object[] obj : content) {
            DataPostDto dto = new DataPostDto();
            BigInteger id = (BigInteger)(obj[0]);
            BigInteger cateid = (BigInteger)(obj[1]);
            dto.setId(id.longValue());
            dto.setCateId(cateid.longValue());
            dto.setCateName(String.valueOf(obj[2]));
            data.add(dto);
        }
        return data;
    }

    @Override
    public Page<DataPostDto> findByCategory(Long cateId, PageRequest pageRequest) {
        String sqlQuery = "SELECT p.id, p.cate_id, c.name"
                +" FROM data_post p LEFT JOIN data_category c ON p.cate_id = c.id"
                +" WHERE p.cate_id = :cateid"
                +" ORDER BY p.created_date desc";
        String sqlCountQuery = "SELECT count(p.id) FROM data_post p WHERE p.cate_id = :cateid";
        HashMap<String, Object> params = new HashMap<>();
        params.put("cateid", cateId);
        Page<Object[]> page = fetchPaging(sqlQuery, sqlCountQuery, params, pageRequest);
        ArrayList<DataPostDto> data = map(page.getContent());
        return new PageImpl<DataPostDto>(data, pageRequest, page.getTotalElements());
    }

    @Override
    public List<DataPostDto> findByCategory(Long cateId) {
        String sqlQuery = "SELECT p.id, p.cate_id, c.name"
                +" FROM data_post p LEFT JOIN data_category c ON p.cate_id = c.id"
                +" WHERE p.cate_id = :cateid"
                +" ORDER BY p.created_date desc";
        HashMap<String, Object> params = new HashMap<>();
        params.put("cateid", cateId);
        List<Object[]> list = fetchList(sqlQuery, params);
        return map(list);
    }
}
