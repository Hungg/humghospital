package com.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.entity.Unit;

@Repository
public interface UnitRepository extends JpaRepository<Unit, Long>{
	
	@Query(value = "SELECT count(a.unit_name) FROM Unit a WHERE a.unit_name = ?1 LIMIT 1", nativeQuery = true)
    int countUnitByName(String unit_name);
    @Query("SELECT c FROM Unit c WHERE c.id IN (?1) ORDER BY c.unit_name ASC, c.id DESC")
    List<Unit> findById(List<Long> accountId);
    
    @Query("SELECT c FROM Unit c WHERE c.unit_name = ?1")
    Unit getByName(String name);
    
    @Query("SELECT c FROM Unit c WHERE c.unit_code = ?1")
    Unit getByUnitCode(String unit_code);
    
    @Query("SELECT c FROM Unit c WHERE c.unit_code IN (?1) ORDER BY c.unit_name ASC, c.id DESC")
    List<Unit> findByUnitcode(List<String> unitcode);

}
