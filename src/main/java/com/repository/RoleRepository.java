package com.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.entity.Role;

@Repository
@Transactional
public interface RoleRepository extends JpaRepository<Role, Long>{
	
	@Query(value="SELECT r.name FROM Role r WHERE r.id = :role_id")
	String getRoleName(@Param("role_id") Long role_id);

}
