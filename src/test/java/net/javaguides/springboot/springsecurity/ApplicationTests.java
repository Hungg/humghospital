package net.javaguides.springboot.springsecurity;

import org.apache.tomcat.util.net.SSLHostConfig;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(loader=AnnotationConfigContextLoader.class, classes = SSLHostConfig.class)
public class ApplicationTests {

	@Test
	public void contextLoads() {
	}

}
